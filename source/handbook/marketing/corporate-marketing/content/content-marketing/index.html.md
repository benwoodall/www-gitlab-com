---
layout: markdown_page
title: "Content Marketing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

The content marketing team is responsible for strategic content creation. The goal
of the content marketing team is to build an engaged audience of people who exhibit 
specific, desirable behaviors (e.g. willingness to share personal data) through 
repeatable and scalable content programs.

The content marketing team's focus is on creating end-to-end content experiences
that bring continued and consistent value to our audience. We value quality over
quanity and seek to deliver content that is backed by research, is useful, and actionable.

## Content marketing calendar

<iframe src="https://calendar.google.com/calendar/embed?src=gitlab.com_6dan8l4r48lrosc1svu42jplnc%40group.calendar.google.com&ctz=America%2FDenver" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

## Process documentation

## Creating a pillar brief 

### Creating content program epics 